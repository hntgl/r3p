# OTA_Update
OTA update keenetic

[Installing OPKG Entware in the router's internal memory](https://help.keenetic.com/hc/en-us/articles/360021888880-Installing-OPKG-Entware-in-the-router-s-internal-memory)

ssh to Router and run command
```
exec sh
```

```
curl -L -s "https://gitlab.com/hntgl/r3p/raw/main/keenetic/otaupdate.sh" --output /opt/otaupdate.sh && chmod +x /opt/otaupdate.sh
```
Run
```
/opt/otaupdate.sh
```