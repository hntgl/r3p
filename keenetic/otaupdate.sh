#!/bin/sh

clear

USER="hntgl"
HOST="https://gitlab.com/hntgl/r3p/raw/main/keenetic"
main_menu() {

  echo ""
  echo "1. OTA Update"
  echo "2. Update Script"
  echo "00. Exit"
  echo ""
  read -p "Select action: " choice

  case "$choice" in
  1) ota_update ;;
  2) script_update ;;
  00) exit ;;
  *)
    echo "Wrong Select. Try again."
    sleep 1
    main_menu
    ;;
  esac
}

script_update() {
  SCRIPT="otaupdate.sh"
  TMP_DIR="/tmp"

  if ! opkg list-installed | grep -q "^curl"; then
    echo "curl package not found, install..."
    opkg install curl
  fi

  curl -L -s "$HOST/$SCRIPT" --output $TMP_DIR/$SCRIPT

  if [ -f "$TMP_DIR/$SCRIPT" ]; then
    mv "$TMP_DIR/$SCRIPT" "/opt/$SCRIPT"
    chmod +x /opt/$SCRIPT
    echo ""
    echo -e "\n+--------------------------------------------------------------+"
    echo -e "|         The script has been successfully updated             |"
    echo -e "+--------------------------------------------------------------+\n"
  else
    printf "Failed to update script.\n"
  fi
  sleep 2
  /opt/$SCRIPT
}


ota_update() {
DIR="firmware"
BIN_FILES=$(curl -s "$HOST/ota.txt" | grep -Po '"name":.*?[^\\]",' | awk -F'"' '{print $4}' | grep ".bin")
if [ -z "$BIN_FILES" ]; then
    printf "There are no files in $DIR directory.\n"
  else
    printf "\nFirmware for Router:\n"
    i=1
    for FILE in $BIN_FILES; do
      printf "$i. $FILE\n"
      i=$((i + 1))
    done

    echo ""
    read -p "Select firmware: " FILE_NUM
    FILE=$(echo "$BIN_FILES" | sed -n "${FILE_NUM}p")
    echo "You Selected $FILE"
    echo "Get firmware..."
    if ! curl -L -s "$HOST/$DIR/$FILE" --output /tmp/$FILE; then
      printf "Failed to get firmware $FILE.\n"
      exit 1
    fi
    echo ""
    if [ -f "/tmp/$FILE" ]; then
      printf "File $FILE downloaded.\n"
    else
      printf "File $FILE was not loaded/found.\n"
      exit 1
    fi
    curl -L -s "$HOST/$DIR/md5sum" --output /tmp/md5sum

    MD5SUM=$(grep "$FILE" /tmp/md5sum | awk '{print $1}')

    FILE_MD5SUM=$(md5sum /tmp/$FILE | awk '{print $1}')

    if [ "$MD5SUM" = "$FILE_MD5SUM" ]; then
      printf "MD5 hash matches.\n"
    else
      printf "MD5 hash does not match"
      echo "Real md5sum - $MD5SUM"
      echo "File md5sum - $FILE_MD5SUM"

      exit 1
    fi
    echo ""
    Firmware="/tmp/$FILE"
    FirmwareName=$(basename "$Firmware")
    read -p "$FirmwareName is selected for update, is that correct? (y/n) " item_rc1
    case "$item_rc1" in
    y | Y)
      echo ""
      mtdSlot="$(grep -w '/proc/mtd' -e 'Firmware_1')"
      result=$(echo "$mtdSlot" | grep -oP '.*(?=:)' | grep -oE '[0-9]+')
      echo "Firmware_1 on mtd$result partition, updating..."
      dd if=$Firmware of=/dev/mtdblock$result
      wait
      echo ""
      mtdSlot2="$(grep -w '/proc/mtd' -e 'Firmware_2')"
      result2=$(echo "$mtdSlot2" | grep -oP '.*(?=:)' | grep -oE '[0-9]+')
      echo "Firmware_2 on mtd$result2 partition, updating..."
      dd if=$Firmware of=/dev/mtdblock$result2
      printf ""
      echo -e "\n+--------------------------------------------------------------+"
      echo -e "|                Firmware updated successfully                  |"
      echo -e "+--------------------------------------------------------------+\n"
      printf ""
      sleep 1
      ;;
    esac
  fi
  read -p "Reboot router? (y/n) " item_rc1
  case "$item_rc1" in
  y | Y)
    echo ""
    reboot
    ;;
  n | N)
    echo ""
    ;;
  *) ;;
  esac
  echo "Return to main menu..."
  sleep 1
  main_menu
  }


main_menu
